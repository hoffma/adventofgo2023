package main

import (
	"bufio"
	"fmt"
	"os"
    "errors"
    s "strings"
)

var digitWords = []string {
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
}

func has_number(substr string) (int, error) {
    for i := 0; i < len(digitWords); i++ {
        if s.Contains(substr, digitWords[i]) {
            return i, nil
        }
    }
    return -1, errors.New(fmt.Sprintf("No number found in %s", substr))
}

func is_num(c byte) (int, error) {
    if (c >= '0' && c <= '9') {
        return int(c - '0'), nil
    }
    return -1, errors.New(fmt.Sprintf("'%c' is not a number", c))
}

func get_first_digit(line string) int {
    a := 0
    b := a

    for i := 0; i < len(line); i++ {
        if d, err := is_num(line[b]); err == nil {
            return d
        }

        if d, err := has_number(line[a:b+1]); err == nil {
            return d
        }

        if (b - a) >= 5 {
            a++
        }
        b++
    }

    return 0
}

func get_last_digit(line string) int {
    a := len(line)-1
    b := a

    for i := a; i >= 0; i-- {
        if d, err := is_num(line[a]); err == nil {
            return d
        }

        if d, err := has_number(line[a:b+1]); err == nil {
            return d
        }

        if (b - a) >= 5 {
            b--
        }
        a--
    }

    return 0
}

func main() {
    // fname := "testdata.txt"
    fname := "data.txt"

    file, err := os.Open(fname)
    if err != nil {
        fmt.Fprintln(os.Stderr, "Error reading ", fname, ": ", err)
    }
    scanner := bufio.NewScanner(file)

    sum := 0

    for scanner.Scan() {
        line := scanner.Text()
        if line == "" {
            break
        }
        value := get_first_digit(line)*10 + get_last_digit(line)
        sum += value
    }

    fmt.Printf("Sum: %d\n", sum)
}
