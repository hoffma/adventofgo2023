package main

import (
    "strconv"
    "fmt"
    "bufio"
    "os"
    s "strings"
)

type cubes struct {
    red     int
    green   int
    blue    int
}

type game struct {
    id      int
    red     int
    green   int
    blue    int
}

func parse_game(line string) game {
    // get ID
    tmp := s.Split(line, ":")
    id, err := strconv.Atoi(s.Split(tmp[0], " ")[1]); 
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error converting ID: %s\n", err)
    }

    tmp_game := new_game(id, 0, 0, 0)

    // get maximum picks
    picks := s.Split(tmp[1], ";")
    for i := 0; i < len(picks); i++ {
        colors := s.Split(picks[i], ",")
        for j := 0; j < len(colors); j++ {
            c := s.Trim(colors[j], " ")
            pair := s.Split(c, " ")

            n, err := strconv.Atoi(pair[0])
            name := pair[1]

            if err != nil {
                fmt.Fprintf(os.Stderr, "Error converting color string: %s, %s\n", colors[j], err)
                fmt.Fprintln(os.Stderr, pair[0])
            }

            switch name {
            case "red":
                if n > tmp_game.red {
                    tmp_game.red = n
                }
            case "green":
                if n > tmp_game.green {
                    tmp_game.green = n
                }
            case "blue":
                if n > tmp_game.blue {
                    tmp_game.blue = n
                }
            }
        }
    }


    return tmp_game
}

func new_game(id int, red int, green int, blue int) game {
    g := game{id, 0, 0, 0}

    return g
}

func check_possible(g *game, c *cubes) int {
    if g.red <= c.red && g.green <= c.green && g.blue <= c.blue {
        return  g.id
    }
    return 0
}

func get_power(g *game) int {
    return g.red * g.green * g.blue
}

func main() {
    fmt.Println("Hello, world!");

    file, err := os.Open("testdata.txt")

    possible_cubes := cubes{12, 13, 14}
    possible_sum := 0
    power_sum := 0

    if err != nil {
        fmt.Fprintln(os.Stderr, "Error reading file ", file, err)
        os.Exit(1)
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        line := scanner.Text()
        gm := parse_game(line)
        possible_sum += check_possible(&gm, &possible_cubes)
        power_sum += get_power(&gm)
    }

    fmt.Printf("possible_sum: %d\n", possible_sum)
    fmt.Printf("power_sum: %d\n", power_sum)
}
