package parser

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
)

type TokenType int
const (
    EOF     TokenType = 0
    DOT               = 1
    SYMBOL            = 2
    PART              = 3
)

type Lexer struct {
    input   string
    pos     int
    readPos int
    ch      byte
}

type Token struct {
    t       TokenType
    x       int
    y       int
    width   int
    val     int
}

func newLexer(line string) Lexer {
    return Lexer{line, 0, 1, line[0]}
}

func consume(l *Lexer) {
    if l.readPos >= len(l.input) {
        l.ch = 0
    } else {
        l.ch = l.input[l.readPos]
    }
    l.pos = l.readPos
    l.readPos += 1
}

func peek(l *Lexer) byte {
    if l.readPos >= len(l.input) {
        return 0
    }

    return l.input[l.readPos]
}

func skipDot(l *Lexer) {
    for l.ch == '.' {
        consume(l)
    }
}

func readNumber(l *Lexer) int {
    num := ""
    for isNum(l.ch) {
        num += string(l.ch)
        consume(l)
    }

    if v, err := strconv.Atoi(num); err != nil {
        fmt.Fprintf(os.Stderr, "Error parsing number '%s': %s", num, err)
        os.Exit(1)
        return 0
    } else {
        return v
    }
}

// var symbols = make(map[int]byte)
// var parts = make(map[int]Part)

func nextToken(l *Lexer, lineNum int) (Token, error){
    switch l.ch {
    case '.':
        skipDot(l)
        return Token{DOT, l.pos, lineNum, 1, 0}, nil
    case 0:
        return Token{EOF, l.pos, lineNum, 0, 0}, errors.New("End of line")
    default:
        if isNum(l.ch) {
            start := l.pos
            num := readNumber(l)
            end := l.pos
            t := Token{PART, start, lineNum, (end - start), num}
            return t, nil
            // parts[lineNum*len(l.input) + start] = p
        } else { // should be a symbol
            /* just skip */
            t := Token{SYMBOL, l.pos, lineNum, 1, int(l.ch)}
            consume(l)
            return t, nil
        }
    }
}

func isNum(c byte) bool {
    return c >= '0' && c <= '9'
}

func max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

func countNeighbours(part *Token, symbols map[int]Token, gridWidth int) int {
    start_x := max(part.x - 1, 0)
    start_y := max(part.y - 1, 0)

    neigh := 0

    for j := start_y; j <= (part.y+1); j++ {
        for i := start_x; i < (part.x+part.width+1); i++ {
            // fmt.Printf("x: %d, y: %d\n", i, j)
            tmp := j*gridWidth + i
            _, pres := symbols[tmp]
            if pres {
                neigh += 1
            }
        }
    }

    return neigh
}

func getGearRatios(parts []Token, symbols map[int]Token, gridWidth int) int {
    ratios := 0
    possible_gears := make(map[int][]int)

    for k := range symbols {
        if symbols[k].val == int('*') {
            possible_gears[k] = make([]int, 0)
        }
    }

    for k := range parts {
        start_x := max(parts[k].x - 1, 0)
        start_y := max(parts[k].y - 1, 0)

        for j := start_y; j <= (parts[k].y+1); j++ {
            for i := start_x; i < (parts[k].x+parts[k].width+1); i++ {
                tmp := j*gridWidth + i
                _, pres := possible_gears[tmp]
                if pres  {
                    possible_gears[tmp] = append(possible_gears[tmp], parts[k].val)
                }
            }
        }
    }

    for k := range possible_gears {
        if len(possible_gears[k]) == 2 {
            ratios += possible_gears[k][0]*possible_gears[k][1]
        }
    }

    // fmt.Println(possible_gears)
    return ratios
}

func Run(fname string) {
    file, err := os.Open(fname)
    if err != nil {
        fmt.Fprintln(os.Stderr, "Error reading file", err)
        os.Exit(1)
    }

    var parts []Token
    var symbols = make(map[int]Token)

    scanner := bufio.NewScanner(file)
    y := 0
    width := 0
    for scanner.Scan() {
        line := scanner.Text()
        l := newLexer(line)
        width = len(line)

        tok, err := nextToken(&l, y)
        for ; err == nil; tok, err = nextToken(&l, y) {
            switch tok.t {
            case PART:
                parts = append(parts, tok)
            case SYMBOL:
                tmp := tok.y*width + tok.x
                symbols[tmp] = tok
            }
        }
        y += 1
    }
    // fmt.Println(parts)
    // fmt.Println(symbols)


    sum := 0
    for i := 0; i < len(parts); i++ {
        n := countNeighbours(&parts[i], symbols, width)
        if n > 0 {
            sum += parts[i].val
        }
        // fmt.Println(parts[i], )
    }

    fmt.Printf("Sum: %d\n", sum)
    fmt.Println(getGearRatios(parts, symbols, width))
}
