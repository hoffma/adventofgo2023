package main

import (
	"d03/parser"
)

func main() {

    parser.Run("testdata.txt")
    parser.Run("data.txt")
}
