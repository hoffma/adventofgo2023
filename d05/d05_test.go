package main

import (
    "testing"
)

func TestP01(t *testing.T) {
    res := P01("testdata.txt")

    if res != 35 {
        t.Errorf("Error on part 1. Got %d, expected 35", res)
    }
}
