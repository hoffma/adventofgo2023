package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	s "strings"
)

type GardenRange struct {
	src  uint64
	dst  uint64
	size uint64
}

type GardenMap struct {
	src_name string
	dst_name string
	ranges   []GardenRange
}

func main() {
	fmt.Println("P01 result: ", P01("data.txt"))
	fmt.Println("P02 result: ", P02("data.txt"))
}

func P01(fname string) uint64 {
	mapRegex := regexp.MustCompile("([a-z]+)-to-([a-z]+) map:")
	rangeRegex := regexp.MustCompile("([0-9]+) ([0-9]+) ([0-9]+)")
	seeds := []uint64{}
	maps := map[string]GardenMap{}
	var curr_map GardenMap
	getLineF := parseFile(fname)

	l, err := getLineF()

	// first line are the seeds
	tmp := s.Split(l, ":")[1]
	tmp = s.TrimSpace(tmp)

	seed_vec := s.Split(tmp, " ")
	for i := range seed_vec {
		if v, err := strconv.Atoi(seed_vec[i]); err == nil {
			seeds = append(seeds, uint64(v))
		} else {
			fmt.Fprintf(os.Stderr, "Error converting %s to int. %s", seed_vec[i], err)
		}
	}

	first := true
	// process the rest
	for l, err = getLineF(); err == nil; l, err = getLineF() {
		if l == "" {
			if !first {
				maps[curr_map.src_name] = curr_map
			}
			first = false
		} else if mapRegex.MatchString(l) {
			tmp := mapRegex.FindStringSubmatch(l)
			src := tmp[1]
			dst := tmp[2]

			curr_map = GardenMap{src, dst, []GardenRange{}}
		} else if rangeRegex.MatchString(l) {
			tmp := rangeRegex.FindStringSubmatch(l)
			dst, _ := strconv.Atoi(tmp[1])
			src, _ := strconv.Atoi(tmp[2])
			rng, _ := strconv.Atoi(tmp[3])
			curr_map.ranges = append(curr_map.ranges, GardenRange{uint64(src), uint64(dst), uint64(rng)})
		} else {
			fmt.Fprintf(os.Stderr, "Line wrong. Got %s\n", l)
			os.Exit(1)
		}
	}
	// save the location mapping
	maps[curr_map.src_name] = curr_map

	min_loc := ^uint64(0)
	for i := range seeds {
		src_id := seeds[i]
		tmp_src := "seed" // starting point

		for tmp_src != "location" {
			dst_id := findDst(src_id, maps[tmp_src].ranges) // get the new source
			tmp_dst := maps[tmp_src].dst_name
			// update
			src_id = dst_id
			tmp_src = tmp_dst
		}
		if src_id < min_loc {
			min_loc = src_id
		}
	}
	return uint64(min_loc)
}

func P02(fname string) uint64 {
	mapRegex := regexp.MustCompile("([a-z]+)-to-([a-z]+) map:")
	rangeRegex := regexp.MustCompile("([0-9]+) ([0-9]+) ([0-9]+)")
	seeds := []uint64{}
	maps := map[string]GardenMap{}
	var curr_map GardenMap
	getLineF := parseFile(fname)

	l, err := getLineF()

	// first line are the seeds
	tmp := s.Split(l, ":")[1]
	tmp = s.TrimSpace(tmp)

	seed_vec := s.Split(tmp, " ")
	for i := range seed_vec {
		if v, err := strconv.Atoi(seed_vec[i]); err == nil {
			seeds = append(seeds, uint64(v))
		} else {
			fmt.Fprintf(os.Stderr, "Error converting %s to int. %s", seed_vec[i], err)
		}
	}

	first := true
	// process the rest
	for l, err = getLineF(); err == nil; l, err = getLineF() {
		if l == "" {
			if !first {
				maps[curr_map.src_name] = curr_map
			}
			first = false
		} else if mapRegex.MatchString(l) {
			tmp := mapRegex.FindStringSubmatch(l)
			src := tmp[1]
			dst := tmp[2]

			curr_map = GardenMap{src, dst, []GardenRange{}}
		} else if rangeRegex.MatchString(l) {
			tmp := rangeRegex.FindStringSubmatch(l)
			dst, _ := strconv.Atoi(tmp[1])
			src, _ := strconv.Atoi(tmp[2])
			rng, _ := strconv.Atoi(tmp[3])
			curr_map.ranges = append(curr_map.ranges, GardenRange{uint64(src), uint64(dst), uint64(rng)})
		} else {
			fmt.Fprintf(os.Stderr, "Line wrong. Got %s\n", l)
			os.Exit(1)
		}
	}
	// save the location mapping
	maps[curr_map.src_name] = curr_map

	// fmt.Println("Seeds: ", seeds)
	// fmt.Println("Maps: ", maps)

	min_loc := ^uint64(0)
	for j := 0; j < len(seeds); j += 2 {
		start := seeds[j]
		end := start + seeds[j+1]
		for i := start; i < end; i++ {

			src_id := i
			tmp_src := "seed" // starting point

			for tmp_src != "location" {
				dst_id := findDst(src_id, maps[tmp_src].ranges) // get the new source
				tmp_dst := maps[tmp_src].dst_name
				// fmt.Printf("seed: %d, dst: (%s, %d), next: %s\n", src_id, tmp_src, dst_id, tmp_dst)
				// update
				src_id = dst_id
				tmp_src = tmp_dst
			}
			if src_id < min_loc {
				min_loc = src_id
			}
			// fmt.Println("Location: ", src_id)
		}
	}
	// fmt.Println("min_loc", min_loc)
	return min_loc
}

func findDst(id uint64, rngs []GardenRange) uint64 {

	for i := range rngs {
		minS := rngs[i].src
		maxS := (rngs[i].src + rngs[i].size)
		if (id >= minS) && (id <= maxS) {
			diff := id - minS
			return (rngs[i].dst + diff)
		}
	}

	return id
}

func matchR(line string, r regexp.Regexp) (bool, []string) {
	// r := regexp.MustCompile("(?P<src>[a-z]+)-to-(?P<dst>[a-z]+) map:")

	return r.MatchString(line), r.FindStringSubmatch(line)
}

func parseFile(fname string) func() (string, error) {
	file, err := os.Open(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading file", err)
	}
	scanner := bufio.NewScanner(file)
	return func() (string, error) {
		if scanner.Scan() {
			return scanner.Text(), nil
		} else {
			return "", errors.New("End of File")
		}
	}
}
