package main

import (
	"reflect"
	"testing"
)

func TestP01(t *testing.T) {
	res := P01("testdata.txt")
	if res != 13 {
		t.Errorf("P01(\"testdata.txt\") failed. Got %d, expected 13", res)
	}
}

func TestP02(t *testing.T) {
	res := P02("testdata.txt")
	if res != 30 {
		t.Errorf("P02(\"testdata.txt\") failed. Got %d, expected 30", res)
	}
}

func TestParseCard(t *testing.T) {
	input := "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"

	c := ParseCard(input)
	tmp_card := Card{2, 0, []int{13, 32, 20, 16, 61}, []int{61, 30, 68, 82, 17, 32, 24, 19}}
	if !reflect.DeepEqual(c, tmp_card) {
		t.Error("expected: ", tmp_card, " got: ", c)
	}
}

func TestGetWinnings(t *testing.T) {
	input := "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"

	c := ParseCard(input)
    win := GetWinnings(&c)
	if win != 2 {
        t.Error("Expected 2 winnings cards, got: ", win)
	}
}
