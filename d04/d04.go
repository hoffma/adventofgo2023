package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	s "strings"
)

type Card struct {
	id      int
    copies  int
	winning []int
	picks   []int
}

func main() {
    fmt.Println("P01: ", P01("data.txt"))
    fmt.Println("P01: ", P02("data.txt"))
}

func eval_p01(cards []Card) int {

    point_sum := 0
    for i := range cards {
        points := 0
        picks := map[int]int{}

        for j := range cards[i].picks {
            p := cards[i].picks[j]
            picks[p] = p
        }

        for j := range cards[i].winning {
            w := cards[i].winning[j]
            _, pres := picks[w]
            if pres {
                if points == 0 {
                    points = 1
                } else {
                    points *= 2
                }
            }
        }
        point_sum += points 
    }

    return point_sum
}

func GetWinnings(card *Card) int {
    points := 0
    picks := map[int]int{}

    for j := range card.picks {
        p := card.picks[j]
        picks[p] = p
    }

    for j := range card.winning {
        w := card.winning[j]
        _, pres := picks[w]
        if pres {
            points += 1
        }
    }

    return points
}

func eval_p02(cards []Card) int {

    for i := range cards {
        p := GetWinnings(&cards[i])
        add_copies := cards[i].copies + 1

        for j := cards[i].id; j < (cards[i].id+p); j++ {
            cards[j].copies += add_copies
        }
    }

    card_count := 0
    for i := range cards {
        card_count += 1 + cards[i].copies
    }

    return card_count
}

func P01(fname string) int {
    var cards []Card

	file, err := os.Open(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading file", err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		c := ParseCard(line)
        cards = append(cards, c)
	}

    points := eval_p01(cards)
	return points
}

func P02(fname string) int {
    var cards []Card

	file, err := os.Open(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error reading file", err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		c := ParseCard(line)
        cards = append(cards, c)
	}

    points := eval_p02(cards)
	return points
}

func ParseCard(line string) Card {
	var winning []int
	var picks []int

	tmp := s.Split(line, ":")
    id, err := strconv.Atoi(s.Join(s.Split(tmp[0], " ")[1:], ""))

	if err != nil {
		fmt.Fprintln(os.Stderr, "Error parsing integer ", err)
	}

	tmp = s.Split(s.TrimSpace(tmp[1]), " | ")

	tmp_w := s.Split(tmp[0], " ")
	for i := range tmp_w {
		if tmp_w[i] == "" {
			continue
		}
		if v, err := strconv.Atoi(s.TrimSpace(tmp_w[i])); err == nil {
			winning = append(winning, v)
		} else {
			fmt.Fprintln(os.Stderr, "Error converting number. ", err)
			os.Exit(1)
		}
	}

	tmp_p := s.Split(tmp[1], " ")
	for i := range tmp_p {
		if tmp_p[i] == "" {
			continue
		}
		if v, err := strconv.Atoi(s.TrimSpace(tmp_p[i])); err == nil {
			picks = append(picks, v)
		} else {
			fmt.Fprintln(os.Stderr, "Error converting number. ", err)
			os.Exit(1)
		}
	}

	return Card{id, 0, winning, picks}
}
