package main

import (
	"fmt"
	"math"
)

func calc_p1(t int, d int) int {
	// function for part one is
	// f(x) = -x^2 + tx - d
	// then just solve and ceil/floor the result

	ti := float64(t)
	di := float64(d)
	x1 := math.Floor((ti / 2.) + math.Sqrt(math.Pow((ti/2.), 2.)-di))
	x2 := math.Ceil((ti / 2.) - math.Sqrt(math.Pow((ti/2.), 2.)-di))

	diff := int(math.Abs(x1-x2) + 1.)

	return diff
}

type Race struct {
	time int
	dist int
}

func solve_p1(data []Race) int {
	res := 1

	for _, r := range data {
		res *= calc_p1(r.time, r.dist)
	}

	return res
}

func main() {
	testdata := []Race{
		{7, 9},
		{15, 40},
		{30, 200},
	}
	data := []Race{
		{54, 446},
		{81, 1292},
		{70, 1035},
		{88, 1007},
	}

	p2_test := []Race{
		{71530, 940200},
	}

	p2_data := []Race{
		{54817088, 446129210351007},
	}

	fmt.Println("p1 testdata: ", solve_p1(testdata))
	fmt.Println("p1 data: ", solve_p1(data))
	fmt.Println("p2 test: ", solve_p1(p2_test))
	fmt.Println("p2 data: ", solve_p1(p2_data))
}
