package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	s "strings"
)

// had to be changed for part 2
const (
	JACK = iota + 2
	TWO
	THREE
	FOUR
	FIVE
	SIX
	SEVEN
	EIGHT
	NINE
	TEN
	// JACK
	QUEEN
	KING
	ACE
)

const (
	HIGH_CARD = iota
	ONE_PAIR
	TWO_PAIR
	THREE_KIND
	FULL_HOUSE
	FOUR_KIND
	FIVE_KIND
)

// If hands are the same, then hand with first high card that differs wins

type Hand struct {
	cards     []int
	hand_type int
	bid       uint64
}

type Node struct {
	val  Hand
	next *Node
}

type List struct {
	head *Node
}

func (l *List) CalcRankSum() uint64 {
	var sum uint64
	var rank uint64
	sum = 0
	rank = 1

	if l.head == nil {
		return 0
	} else {
		p := l.head
		// fmt.Printf("%4d * %d\n", p.val.bid, rank)
		sum += rank * p.val.bid
		rank += 1
		for p.next != nil {
			p = p.next
			// fmt.Printf("%4d * %d\n", p.val.bid, rank)
			sum += rank * p.val.bid
			rank += 1
		}
	}

	return sum
}

func (l *List) Insert(h Hand) {
	hand_node := &Node{val: h, next: nil}
	if l.head == nil {
		l.head = hand_node
	} else {
		p := l.head
		if p.val.Wins(&hand_node.val) {
			// fmt.Println("p.next wins")
			tmp := p
			l.head = hand_node
			p = l.head
			p.next = tmp
			return
		}
		for p.next != nil {
			// fmt.Println("p.next: ", p.next.val.ToString(), ", h: ", hand_node.val.ToString())
			if p.next.val.Wins(&hand_node.val) {
				// fmt.Println("p.next wins")
				tmp := p.next
				p.next = hand_node
				p.next.next = tmp
				return
			} else {
				p = p.next
			}
		}
		p.next = hand_node
	}
}

func (l *List) ToString() string {
	p := l.head
	out := "[\n"
	for p != nil {
		if p.next != nil {
			out += fmt.Sprintf("%s ->\n", p.val.ToString())
		} else {
			out += fmt.Sprintf("%s\n]", p.val.ToString())
		}
		p = p.next
	}
	return out
}

func main() {
	fname := "data.txt"

	fmt.Println("P1: ", P1(fname))
	fmt.Println("P2: ", P2(fname))
}

func P1(fname string) uint64 {
	hands := parseHands(fname)

	list := List{}

	for i := range hands {
		list.Insert(hands[i])
	}
	return list.CalcRankSum()
}

func P2(fname string) uint64 {
	hands := parseHandsP2(fname)

	list := List{}

	for i := range hands {
		list.Insert(hands[i])
	}
	// fmt.Println("List: ", list.ToString())
	return list.CalcRankSum()
}

func (h *Hand) Wins(ch *Hand) bool {
	if h.hand_type > ch.hand_type {
		return true
	} else if h.hand_type == ch.hand_type {
		for i := range h.cards {
			if h.cards[i] == ch.cards[i] {
				continue
			} else {
				return h.cards[i] > ch.cards[i]
			}
		}
	}
	return false
}

func (h *Hand) ToString() string {
	out := fmt.Sprintf("%s, type: %s, bid: %d", CardsToString(h.cards), handvalueToString(h.hand_type), h.bid)
	return out
}

func parseHandsP2(fname string) []Hand {
	var hands []Hand
	file, err := os.Open(fname)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()

		tmp := s.Split(line, " ")
		hand := getHand(tmp[0])
		handValue := getHandValueP2(hand)
		bid, err := strconv.Atoi(tmp[1])
		if err != nil {
			panic(err)
		}
		hands = append(hands, Hand{hand, handValue, uint64(bid)})
	}

	return hands
}

func parseHands(fname string) []Hand {
	var hands []Hand
	file, err := os.Open(fname)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()

		tmp := s.Split(line, " ")
		hand := getHand(tmp[0])
		handValue := getHandValue(hand)
		bid, err := strconv.Atoi(tmp[1])
		if err != nil {
			panic(err)
		}
		hands = append(hands, Hand{hand, handValue, uint64(bid)})
	}

	return hands
}

func getHand(hand string) []int {
	var hand_out []int

	for i := 0; i < len(hand); i++ {
		switch hand[i] {
		case '2':
			hand_out = append(hand_out, TWO)
		case '3':
			hand_out = append(hand_out, THREE)
		case '4':
			hand_out = append(hand_out, FOUR)
		case '5':
			hand_out = append(hand_out, FIVE)
		case '6':
			hand_out = append(hand_out, SIX)
		case '7':
			hand_out = append(hand_out, SEVEN)
		case '8':
			hand_out = append(hand_out, EIGHT)
		case '9':
			hand_out = append(hand_out, NINE)
		case 'T':
			hand_out = append(hand_out, TEN)
		case 'J':
			hand_out = append(hand_out, JACK)
		case 'Q':
			hand_out = append(hand_out, QUEEN)
		case 'K':
			hand_out = append(hand_out, KING)
		case 'A':
			hand_out = append(hand_out, ACE)
		}
	}
	return hand_out
}

func getHandValueP2(hand []int) int {
	tmp := map[int]int{}

	for i := range hand {
		if _, pres := tmp[hand[i]]; pres == true {
			tmp[hand[i]] += 1
		} else {
			tmp[hand[i]] = 1
		}
	}
	values := []int{}
	jacks := tmp[JACK]
	tmp[JACK] = 0
	for _, v := range tmp {
		values = append(values, v)
	}
	// slices.Sort(values)
	sort.Slice(values, func(i, j int) bool {
		return values[i] > values[j]
	})

	if (values[0] + jacks) > 5 {
		values[0] = 5
	} else {
		values[0] += jacks
	}

	if values[0] == 5 {
		return FIVE_KIND
	} else if values[0] == 4 {
		return FOUR_KIND
	} else if values[0] == 3 && values[1] == 2 {
		return FULL_HOUSE
	} else if values[0] == 3 {
		return THREE_KIND
	} else if values[0] == 2 && values[1] == 2 {
		return TWO_PAIR
	} else if values[0] == 2 {
		return ONE_PAIR
	} else {
		return HIGH_CARD
	}
}

func getHandValue(hand []int) int {
	tmp := map[int]int{}

	for i := range hand {
		if _, pres := tmp[hand[i]]; pres == true {
			tmp[hand[i]] += 1
		} else {
			tmp[hand[i]] = 1
		}
	}
	values := []int{}
	for _, v := range tmp {
		values = append(values, v)
	}
	// slices.Sort(values)
	sort.Slice(values, func(i, j int) bool {
		return values[i] > values[j]
	})

	if values[0] == 5 {
		return FIVE_KIND
	} else if values[0] == 4 {
		return FOUR_KIND
	} else if values[0] == 3 && values[1] == 2 {
		return FULL_HOUSE
	} else if values[0] == 3 {
		return THREE_KIND
	} else if values[0] == 2 && values[1] == 2 {
		return TWO_PAIR
	} else if values[0] == 2 {
		return ONE_PAIR
	} else {
		return HIGH_CARD
	}
}

func CardsToString(cards []int) string {
	out := ""
	for i := range cards {
		switch cards[i] {
		case ACE:
			out += "A"
		case KING:
			out += "K"
		case QUEEN:
			out += "Q"
		case JACK:
			out += "J"
		case TEN:
			out += "T"
		case NINE:
			out += "9"
		case EIGHT:
			out += "8"
		case SEVEN:
			out += "7"
		case SIX:
			out += "6"
		case FIVE:
			out += "5"
		case FOUR:
			out += "4"
		case THREE:
			out += "3"
		case TWO:
			out += "2"
		}
	}
	return out
}

func handvalueToString(handvalue int) string {
	switch handvalue {
	case HIGH_CARD:
		return "High Card"
	case ONE_PAIR:
		return "One Pair"
	case TWO_PAIR:
		return "Two Pair"
	case THREE_KIND:
		return "Three of a Kind"
	case FULL_HOUSE:
		return "Full House"
	case FOUR_KIND:
		return "Four of a Kind"
	case FIVE_KIND:
		return "Five of a kind"
	}

	return "Unknown Hand"
}

func handToString(hand *Hand) string {
	cards := fmt.Sprint(hand.cards)
	out := fmt.Sprintf("%s, type: %s, bid: %d", cards, handvalueToString(hand.hand_type), hand.bid)
	return out
}
